#tc_02
=====
Operacje na tablicach danych / tablicach obiektów

##Podstawowe komendy i ich zastosowanie:

1. Utworzenie tabeli create table
### utworzenie w marioDB
 1. users (kolumny – id, name, added) gdzie id jest kluczem głównym pobieranym z sekwencji, name – wartością tekstową a added domyślnie ustawiany na aktualną datę),
 2. Utwórz tabelę owoce – fruits (id, name) gdzie id jest kluczem głównym a name wartością tekstową
### mapowanie:  
```
php bin/console doctrine:mapping:import --force AppBundle xml
```
### budowa entity
```
php bin/console doctrine:mapping:convert annotation ./src
```
### update database
```
 php bin/console doctrine:schema:update --force
```

2. Dodanie rekordu do tabeli insert
3. Zmiana danych w tabeli update
4. Pobranie danych konstrukcja select
5. Łączenie tabel konstrukcja join, inner join, left join, right join
6. Zawężenie rekordów słowo kluczowe „where”
7. wykorzystanie podzapytania konstrukcja „in”
8. Słowa kluczowe „order by” oraz „limit”
9. Grupowanie i agregacja wyników słowa kluczowe „group by”

##Pytania do zadania:
1. Jaka jest różnica między join, inner join, left join, right join
2. Jakie funkcje agregujące udało się sprawdzić.
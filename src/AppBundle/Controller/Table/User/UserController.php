<?php

namespace AppBundle\Controller\Table\User;

use AppBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function indexAction(Request $request)
    {
        $id = (int)$request->get('id');

        if($id == 0) {
            return $this->redirectToRoute('users_list');
        }

        $user = $this->getDoctrine()
            ->getRepository(Users::class)
            ->find($id);

        $title = 'User | One ' . $id;

        return $this->render('@App/Table/users.html.twig', [
            'title' => $title,
        ]);
    }


}
<?php

namespace AppBundle\Controller\Table\User;

use AppBundle\Entity\Users;
use AppBundle\Form\User\FormUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserCreateController extends Controller
{
    public function indexAction(Request $request)
    {
        $title = 'Users | Create';

        $user = new Users();

        $form = $this->createForm(FormUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $user = $this->addCreateTime($data);

            /*$entityManager = $this->getDoctrine()->getManager();
            $users = new Users();
            $users->setName('abc02');
            $users->setAdded(new \DateTime('now'));
            $entityManager->persist($users);
            $entityManager->flush();*/

            return $this->redirectToRoute('users_list');
        }

        return $this->render('@App/Table/user.create.html.twig', [
            'title' => $title,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Users $user
     * @return Users
     */
    private function addCreateTime(Users $user)
    {
        $user->setAdded(new \DateTime('now'));

        return $user;
    }
}
<?php

namespace AppBundle\Controller\Table\User;

use AppBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersListController extends Controller
{
    public function indexAction()
    {
        $title = 'Users | List';

        $users = $this->getDoctrine()
            ->getRepository(Users::class)
            ->findAll();

        return $this->render('@App/Table/users.html.twig', [
           'title' => $title,
            'users' => $users,
        ]);
    }
}
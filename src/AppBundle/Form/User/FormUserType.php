<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FormUserType extends AbstractType
{
    public function buidForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
               'label' => 'name',
               'required' => true,
                'attr' => ['maxlength' => 255]
            ]);

        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'submit',
                'attr' => [
                    'style' => 'margin-top: 10px',
                    'class' => 'btn btn-outline btn-xl js-scroll-trigger',
                ]

            ]);
    }
}